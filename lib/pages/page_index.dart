import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('등산 사랑회 소개',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
             print('menu button is clicked');
          },
        ),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: const EdgeInsets.only(left: 15, top: 2, right: 2, bottom: 15),
                  child: const Text('소개',
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                ),
              ),
              Container(
                child: Image.asset('assets/climb1.jpg'
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: const EdgeInsets.only(left: 15, top: 14, right: 4, bottom: 15),
                  child: const Text('등산 사랑회',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: const EdgeInsets.only(left: 15, top: 4, right: 4, bottom: 0),
                  child: const Text('회비 : 10,000'
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: const EdgeInsets.only(left: 15, top: 0, right: 4, bottom: 15),
                  child: const Text('매주 토요일 새벽 6시 출발'
                  ),
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.only(left: 15, top: 4, right: 4, bottom: 15),
                        child: const Text('주요 멤버',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                        ),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Image.asset('assets/climb2.jpg',
                                  width: 150,
                                  height: 200,
                                  fit: BoxFit.fill,
                                ),
                                Column(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(left: 0, top: 10, right: 60, bottom: 15),
                                      child: const Text('회장 홍길동'
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: [
                                Image.asset('assets/climb3.jpg',
                                width: 150,
                                  height: 200,
                                  fit: BoxFit.fill,
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 0, top: 10, right: 40, bottom: 15),
                                  child: const Text('부회장 고길동'
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}